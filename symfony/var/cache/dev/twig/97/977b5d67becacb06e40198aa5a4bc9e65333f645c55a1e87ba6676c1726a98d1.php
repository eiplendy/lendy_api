<?php

/* YLCoreAppBundle:Default:myHome.html.twig */
class __TwigTemplate_57e94bc96024a9e6cda179c27cf42de9428ace99961fc17e6ec4b61eb06dbf59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:myHome.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:myHome.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"author\" content=\"ThemeStarz\">

    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Poppins:400,500,600\">
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/bootstrap/css/bootstrap.min.css "), "html", null, true);
        echo " \">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/font-awesome/css/fontawesome-all.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/style.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/owl.carousel.min.css"), "html", null, true);
        echo "\">

    <title>Lendy</title>

</head>

<body data-spy=\"scroll\" data-target=\".navbar\" class=\"\">
<!--NAVIGATION ******************************************************************************************-->
<nav class=\"navbarHome  navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element\" data-bg-color=\"#1a1360\">
    <div class=\"container\">
        <a class=\"navbar-brand\" href=\"#page-top\">
            <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/logopetit.png"), "html", null, true);
        echo "\" alt=\"\">
        </a>
        <!--end navbar-brand-->
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <!--end navbar-toggler-->
        <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
            <div class=\"navbar-nav ml-auto\">
                <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_myLogin");
        echo "\">Se connecter</a>
                <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_register_choice");
        echo "\">S'inscrire</a>
            </div>
            <!--end navbar-nav-->
        </div>
        <!--end collapse-->
    </div>
    <!--end container-->
</nav>
    <div class=\"ts-page-wrapper\" id=\"page-top\">
        <!--*********************************************************************************************************-->
        <!--************ HERO ***************************************************************************************-->
        <!--*********************************************************************************************************-->
        <header id=\"ts-hero\" class=\"ts-full-screen\" data-bg-parallax=\"scroll\" data-bg-parallax-speed=\"3\">



            <!--HERO CONTENT ****************************************************************************************-->
            <div class=\"container align-self-center\">
                <div class=\"row align-items-center\">
                    <div class=\"col-sm-7\">
                        <h3 class=\"ts-opacity__50\">Découvrez</h3>
                        <h1>Le partage automobile, réinventé</h1>
                        <a href=\"#how-it-works\" class=\"btn btn-light btn-lg ts-scroll\">En savoir plus</a>
                    </div>
                </div>
                <!--end row-->
            </div>
            <!--end container-->

            <div class=\"ts-background\" data-bg-image-opacity=\".6\" data-bg-parallax=\"scroll\" data-bg-parallax-speed=\"3\">
                <div class=\"ts-svg ts-z-index__2\">
                    <img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/svg/wave-static-02.svg"), "html", null, true);
        echo "\" class=\"w-100 position-absolute ts-bottom__0\">
                    <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/svg/wave-static-01.svg"), "html", null, true);
        echo "\" class=\"w-100 position-absolute ts-bottom__0\">
                </div>
                <div class=\"owl-carousel ts-hero-slider\" data-owl-loop=\"1\">
                    <div class=\"ts-background-image ts-parallax-element\" data-bg-color=\"#d24354\" data-bg-image=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/slider_home.jpg"), "html", null, true);
        echo "\" data-bg-blend-mode=\"multiply\"></div>
                    ";
        // line 70
        echo "                </div>
            </div>

        </header>
        <!--end #hero-->

        <!--*********************************************************************************************************-->
        <!--************ CONTENT ************************************************************************************-->
        <!--*********************************************************************************************************-->
        <main id=\"ts-content\">

            <!--HOW IT WORKS ****************************************************************************************-->
            <section id=\"how-it-works\" class=\"ts-block text-center\">
                <div class=\"container\">
                    <div class=\"ts-title\">
                        <h2>Demarrez avec Lendy</h2>
                    </div>
                    <!--end ts-title-->
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-md-6 col-xl-6\">
                            <figure data-animate=\"ts-fadeInUp\">
                                <figure class=\"icon mb-5 p-2\">
                                    <img src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/wheel.png"), "html", null, true);
        echo "\" alt=\"\">
                                    <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/svg/organic-shape-01.svg"), "html", null, true);
        echo "\"></div>
                                </figure>
                                <h4>Je suis chauffeur</h4>
                                <p>
                                    Vous avez du temps à consacrer à la plateforme, et vous voulez récuperer un véhicule en échange?
                                </p>
                                <a href=\"";
        // line 99
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_register");
        echo "\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class=\"col-sm-6 col-md-6 col-xl-6\">
                            <figure data-animate=\"ts-fadeInUp\" data-delay=\"0.1s\">
                                <figure class=\"icon mb-5 p-2\">
                                    <img src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/handshake.png"), "html", null, true);
        echo "\" alt=\"\">
                                    <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/svg/organic-shape-02.svg"), "html", null, true);
        echo "\"></div>
                                </figure>
                                <h4>Je suis prêteur</h4>
                                <p>
                                    Vous avez un véhicule à dispotition mais vous l'utilsez rarement. Prêtez le en échange d'un chauffeur !
                                </p>
                                <a href=\"";
        // line 113
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_register");
        echo "\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                            </figure>
                        </div>
                        <!--end col-xl-4-->

                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </section>
            <!--END HOW IT WORKS ************************************************************************************-->

<!--WHAT IS LENDY ************************************************************************************-->
<section id=\"what-is-appstorm\" class=\"ts-block\">
    <div class=\"container\">
        <div class=\"ts-title\">
            <h2>Lendy c'est quoi?</h2>
        </div>
        <!--end ts-title-->
        <div class=\"row\">
            <div class=\"col-md-5 col-xl-5\" data-animate=\"ts-fadeInUp\" data-offset=\"100\">
                <p>
                    Votre voiture reste dans le garage car vous ne pouvez plus
                    l'utiliser, alors que vous avez des besoins de déplacement ponctuels ?
                    Vous avez besoin d'une voiture, sans les moyens de l'acquérir, mais vous avez du temps ?
                    A travers une plate-forme web et mobile, Lendy met en relation ces
                    personnes et permet de résoudre leurs besoins.
                </p>

                <a href=\"";
        // line 142
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_register_choice");
        echo "\" class=\"btn btn-primary mb-4 ts-scroll\">Je m'inscris!</a>
            </div>
            <!--end col-xl-5-->
            <div class=\"col-md-7 col-xl-7 text-center\" data-animate=\"ts-fadeInUp\" data-delay=\"0.1s\" data-offset=\"100\">
                <div class=\"px-3\">
                    <img src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/covoit.jpg"), "html", null, true);
        echo "\" class=\"mw-100 ts-shadow__lg ts-border-radius__md\" alt=\"\">
                </div>
            </div>
            <!--end col-xl-7-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--END WHAT IS LENDY ********************************************************************************-->


<section id=\"our-clients\" class=\"ts-block text-center\">
    <div class=\"container\">
        <div class=\"ts-title\">
            <h2>Quelques témoignages de nos utilisateurs</h2>
        </div>
        <!--end ts-title-->
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"owl-carousel ts-carousel-blockquote\" data-owl-dots=\"1\" data-animate=\"ts-zoomInShort\">
                    <blockquote class=\"blockquote\">
                        <!--person image-->
                        <figure>
                            <aside>
                                <i class=\"fa fa-quote-right\"></i>
                            </aside>
                            <div class=\"ts-circle__lg\" data-bg-image=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/bond.jpg"), "html", null, true);
        echo "\"></div>
                        </figure>
                        <!--end person image-->
                        <!--cite-->
                        <p>
                            J'ai pu récuperer un véhicule grâce à Lendy et completer ma mission.
                        </p>
                        <!--end cite-->
                        <!--person name-->
                        <footer class=\"blockquote-footer\">
                            <h4>James Bond</h4>
                            <h6>Agent secret</h6>
                        </footer>
                        <!--end person name-->
                    </blockquote>
                    <!--end blockquote-->
                    <blockquote class=\"blockquote\">
                        <!--person image-->
                        <figure>
                            <aside>
                                <i class=\"fa fa-quote-right\"></i>
                            </aside>
                            <div class=\"ts-circle__lg\" data-bg-image=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/brucewayne.jpg"), "html", null, true);
        echo "\"></div>
                        </figure>
                        <!--end person image-->
                        <!--cite-->
                        <p>
                            Je prête ma bat-mobile sur Lendy !
                        </p>
                        <!--end cite-->
                        <!--person name-->
                        <footer class=\"blockquote-footer\">
                            <h4>Bruce Wayne</h4>
                            <h6>PDG de Wayne Company</h6>
                        </footer>
                        <!--end person name-->
                    </blockquote>
                    <!--end blockquote-->
                </div>
            </div>
        </div>

    </div>
</section>


<section id=\"download\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"p-5 text-center ts-border-radius__round-shape ts-shadow__lg\" data-bg-color=\"#1b1464\">
                    <div class=\"bg-white p-5 ts-border-radius__round-shape\">
                        <div class=\"row\">
                            <div class=\"col-md-6\">
                                <a href=\"#\">
                                    <img src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/download-appstore.png"), "html", null, true);
        echo "\" class=\"mw-100 py-3\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-6\">
                                <a href=\"#\">
                                    <img src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/download-play.png"), "html", null, true);
        echo "\" class=\"mw-100 py-3\" alt=\"\">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class=\"position-absolute ts-top__0 ts-bottom__0 m-auto w-100 ts-z-index__-1\">
</section>


</main>
<!--end #content-->

<!--*********************************************************************************************************-->
<!--************ FOOTER *************************************************************************************-->
<!--*********************************************************************************************************-->
<footer id=\"ts-footer\">

";
        // line 256
        echo "
<section id=\"contact\" class=\"ts-separate-bg-element down-footer\" data-bg-image=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/bg-desk.jpg"), "html", null, true);
        echo "\" data-bg-image-opacity=\".1\" data-bg-color=\"#1b1464\">
    <div class=\"container\">
        <div class=\"ts-box mb-0 p-5 ts-mt__n-10\">
            <div class=\"row\">
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Questions récurrentes</a>
                </div>
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Mentions légales</a>
                </div>
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Condition générales d'utilisation</a>
                </div>
            </div>
            <!--end row-->
        </div>
        <!--end ts-box-->
    </div>
    <!--end container-->
</section>

</footer>
<!--end #footer-->
</div>
<!--end page-->


</body>
</html>
<script>
    if( document.getElementsByClassName(\"ts-full-screen\").length ) {
        document.getElementsByClassName(\"ts-full-screen\")[0].style.height = window.innerHeight + \"px\";
    }
</script>
<script src=\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/popper.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/imagesloaded.pkgd.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/isInViewport.jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.particleground.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 297
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/scrolla.jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 300
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-validate.bootstrap-tooltip.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js\"></script>
<script src=\"";
        // line 302
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.wavify.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/custom.js"), "html", null, true);
        echo "\"></script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "YLCoreAppBundle:Default:myHome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  434 => 304,  429 => 302,  424 => 300,  420 => 299,  416 => 298,  412 => 297,  408 => 296,  404 => 295,  400 => 294,  396 => 293,  392 => 292,  388 => 291,  351 => 257,  348 => 256,  324 => 234,  316 => 229,  280 => 196,  255 => 174,  225 => 147,  217 => 142,  185 => 113,  176 => 107,  172 => 106,  162 => 99,  153 => 93,  149 => 92,  125 => 70,  121 => 68,  115 => 65,  111 => 64,  77 => 33,  73 => 32,  61 => 23,  47 => 12,  43 => 11,  39 => 10,  35 => 9,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"author\" content=\"ThemeStarz\">

    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Poppins:400,500,600\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/bootstrap/css/bootstrap.min.css ') }} \">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/font-awesome/css/fontawesome-all.min.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/style.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/owl.carousel.min.css') }}\">

    <title>Lendy</title>

</head>

<body data-spy=\"scroll\" data-target=\".navbar\" class=\"\">
<!--NAVIGATION ******************************************************************************************-->
<nav class=\"navbarHome  navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element\" data-bg-color=\"#1a1360\">
    <div class=\"container\">
        <a class=\"navbar-brand\" href=\"#page-top\">
            <img src=\"{{ asset('assets/img/logopetit.png') }}\" alt=\"\">
        </a>
        <!--end navbar-brand-->
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <!--end navbar-toggler-->
        <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
            <div class=\"navbar-nav ml-auto\">
                <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"{{ path('yl_core_app_myLogin') }}\">Se connecter</a>
                <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"{{ path('yl_core_app_register_choice') }}\">S'inscrire</a>
            </div>
            <!--end navbar-nav-->
        </div>
        <!--end collapse-->
    </div>
    <!--end container-->
</nav>
    <div class=\"ts-page-wrapper\" id=\"page-top\">
        <!--*********************************************************************************************************-->
        <!--************ HERO ***************************************************************************************-->
        <!--*********************************************************************************************************-->
        <header id=\"ts-hero\" class=\"ts-full-screen\" data-bg-parallax=\"scroll\" data-bg-parallax-speed=\"3\">



            <!--HERO CONTENT ****************************************************************************************-->
            <div class=\"container align-self-center\">
                <div class=\"row align-items-center\">
                    <div class=\"col-sm-7\">
                        <h3 class=\"ts-opacity__50\">Découvrez</h3>
                        <h1>Le partage automobile, réinventé</h1>
                        <a href=\"#how-it-works\" class=\"btn btn-light btn-lg ts-scroll\">En savoir plus</a>
                    </div>
                </div>
                <!--end row-->
            </div>
            <!--end container-->

            <div class=\"ts-background\" data-bg-image-opacity=\".6\" data-bg-parallax=\"scroll\" data-bg-parallax-speed=\"3\">
                <div class=\"ts-svg ts-z-index__2\">
                    <img src=\"{{ asset('assets/svg/wave-static-02.svg') }}\" class=\"w-100 position-absolute ts-bottom__0\">
                    <img src=\"{{ asset('assets/svg/wave-static-01.svg') }}\" class=\"w-100 position-absolute ts-bottom__0\">
                </div>
                <div class=\"owl-carousel ts-hero-slider\" data-owl-loop=\"1\">
                    <div class=\"ts-background-image ts-parallax-element\" data-bg-color=\"#d24354\" data-bg-image=\"{{ asset('assets/img/slider_home.jpg') }}\" data-bg-blend-mode=\"multiply\"></div>
                    {#<div class=\"ts-background-image ts-parallax-element\" data-bg-color=\"#d24354\" data-bg-image=\"assets/img/bg-girl-02.jpg\" data-bg-blend-mode=\"multiply\"></div>#}
                </div>
            </div>

        </header>
        <!--end #hero-->

        <!--*********************************************************************************************************-->
        <!--************ CONTENT ************************************************************************************-->
        <!--*********************************************************************************************************-->
        <main id=\"ts-content\">

            <!--HOW IT WORKS ****************************************************************************************-->
            <section id=\"how-it-works\" class=\"ts-block text-center\">
                <div class=\"container\">
                    <div class=\"ts-title\">
                        <h2>Demarrez avec Lendy</h2>
                    </div>
                    <!--end ts-title-->
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-md-6 col-xl-6\">
                            <figure data-animate=\"ts-fadeInUp\">
                                <figure class=\"icon mb-5 p-2\">
                                    <img src=\"{{ asset('assets/img/wheel.png') }}\" alt=\"\">
                                    <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"{{ asset('assets/svg/organic-shape-01.svg') }}\"></div>
                                </figure>
                                <h4>Je suis chauffeur</h4>
                                <p>
                                    Vous avez du temps à consacrer à la plateforme, et vous voulez récuperer un véhicule en échange?
                                </p>
                                <a href=\"{{ path('yl_core_app_register') }}\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class=\"col-sm-6 col-md-6 col-xl-6\">
                            <figure data-animate=\"ts-fadeInUp\" data-delay=\"0.1s\">
                                <figure class=\"icon mb-5 p-2\">
                                    <img src=\"{{ asset('assets/img/handshake.png') }}\" alt=\"\">
                                    <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"{{ asset('assets/svg/organic-shape-02.svg') }}\"></div>
                                </figure>
                                <h4>Je suis prêteur</h4>
                                <p>
                                    Vous avez un véhicule à dispotition mais vous l'utilsez rarement. Prêtez le en échange d'un chauffeur !
                                </p>
                                <a href=\"{{ path('yl_core_app_register') }}\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                            </figure>
                        </div>
                        <!--end col-xl-4-->

                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </section>
            <!--END HOW IT WORKS ************************************************************************************-->

<!--WHAT IS LENDY ************************************************************************************-->
<section id=\"what-is-appstorm\" class=\"ts-block\">
    <div class=\"container\">
        <div class=\"ts-title\">
            <h2>Lendy c'est quoi?</h2>
        </div>
        <!--end ts-title-->
        <div class=\"row\">
            <div class=\"col-md-5 col-xl-5\" data-animate=\"ts-fadeInUp\" data-offset=\"100\">
                <p>
                    Votre voiture reste dans le garage car vous ne pouvez plus
                    l'utiliser, alors que vous avez des besoins de déplacement ponctuels ?
                    Vous avez besoin d'une voiture, sans les moyens de l'acquérir, mais vous avez du temps ?
                    A travers une plate-forme web et mobile, Lendy met en relation ces
                    personnes et permet de résoudre leurs besoins.
                </p>

                <a href=\"{{ path('yl_core_app_register_choice') }}\" class=\"btn btn-primary mb-4 ts-scroll\">Je m'inscris!</a>
            </div>
            <!--end col-xl-5-->
            <div class=\"col-md-7 col-xl-7 text-center\" data-animate=\"ts-fadeInUp\" data-delay=\"0.1s\" data-offset=\"100\">
                <div class=\"px-3\">
                    <img src=\"{{ asset('assets/img/covoit.jpg') }}\" class=\"mw-100 ts-shadow__lg ts-border-radius__md\" alt=\"\">
                </div>
            </div>
            <!--end col-xl-7-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--END WHAT IS LENDY ********************************************************************************-->


<section id=\"our-clients\" class=\"ts-block text-center\">
    <div class=\"container\">
        <div class=\"ts-title\">
            <h2>Quelques témoignages de nos utilisateurs</h2>
        </div>
        <!--end ts-title-->
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"owl-carousel ts-carousel-blockquote\" data-owl-dots=\"1\" data-animate=\"ts-zoomInShort\">
                    <blockquote class=\"blockquote\">
                        <!--person image-->
                        <figure>
                            <aside>
                                <i class=\"fa fa-quote-right\"></i>
                            </aside>
                            <div class=\"ts-circle__lg\" data-bg-image=\"{{ asset('assets/img/bond.jpg') }}\"></div>
                        </figure>
                        <!--end person image-->
                        <!--cite-->
                        <p>
                            J'ai pu récuperer un véhicule grâce à Lendy et completer ma mission.
                        </p>
                        <!--end cite-->
                        <!--person name-->
                        <footer class=\"blockquote-footer\">
                            <h4>James Bond</h4>
                            <h6>Agent secret</h6>
                        </footer>
                        <!--end person name-->
                    </blockquote>
                    <!--end blockquote-->
                    <blockquote class=\"blockquote\">
                        <!--person image-->
                        <figure>
                            <aside>
                                <i class=\"fa fa-quote-right\"></i>
                            </aside>
                            <div class=\"ts-circle__lg\" data-bg-image=\"{{ asset('assets/img/brucewayne.jpg') }}\"></div>
                        </figure>
                        <!--end person image-->
                        <!--cite-->
                        <p>
                            Je prête ma bat-mobile sur Lendy !
                        </p>
                        <!--end cite-->
                        <!--person name-->
                        <footer class=\"blockquote-footer\">
                            <h4>Bruce Wayne</h4>
                            <h6>PDG de Wayne Company</h6>
                        </footer>
                        <!--end person name-->
                    </blockquote>
                    <!--end blockquote-->
                </div>
            </div>
        </div>

    </div>
</section>


<section id=\"download\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"p-5 text-center ts-border-radius__round-shape ts-shadow__lg\" data-bg-color=\"#1b1464\">
                    <div class=\"bg-white p-5 ts-border-radius__round-shape\">
                        <div class=\"row\">
                            <div class=\"col-md-6\">
                                <a href=\"#\">
                                    <img src=\"{{ asset('assets/img/download-appstore.png') }}\" class=\"mw-100 py-3\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-6\">
                                <a href=\"#\">
                                    <img src=\"{{ asset('assets/img/download-play.png') }}\" class=\"mw-100 py-3\" alt=\"\">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class=\"position-absolute ts-top__0 ts-bottom__0 m-auto w-100 ts-z-index__-1\">
</section>


</main>
<!--end #content-->

<!--*********************************************************************************************************-->
<!--************ FOOTER *************************************************************************************-->
<!--*********************************************************************************************************-->
<footer id=\"ts-footer\">

{#<div class=\"map ts-height__600px\" id=\"map\"></div>#}

<section id=\"contact\" class=\"ts-separate-bg-element down-footer\" data-bg-image=\"{{ asset('assets/img/bg-desk.jpg') }}\" data-bg-image-opacity=\".1\" data-bg-color=\"#1b1464\">
    <div class=\"container\">
        <div class=\"ts-box mb-0 p-5 ts-mt__n-10\">
            <div class=\"row\">
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Questions récurrentes</a>
                </div>
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Mentions légales</a>
                </div>
                <div class=\"col-sm-4 col-md-4 col-lg-4\" style=\"text-align: center;\">
                    <a href=\"#\">Condition générales d'utilisation</a>
                </div>
            </div>
            <!--end row-->
        </div>
        <!--end ts-box-->
    </div>
    <!--end container-->
</section>

</footer>
<!--end #footer-->
</div>
<!--end page-->


</body>
</html>
<script>
    if( document.getElementsByClassName(\"ts-full-screen\").length ) {
        document.getElementsByClassName(\"ts-full-screen\")[0].style.height = window.innerHeight + \"px\";
    }
</script>
<script src=\"{{ asset('assets/js/jquery-3.3.1.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/popper.min.js') }}\"></script>
<script src=\"{{ asset('assets/bootstrap/js/bootstrap.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/imagesloaded.pkgd.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/isInViewport.jquery.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery.particleground.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/owl.carousel.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/scrolla.jquery.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery.validate.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery-validate.bootstrap-tooltip.min.js') }}\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js\"></script>
<script src=\"{{ asset('assets/js/jquery.wavify.js') }}\"></script>

<script src=\"{{ asset('assets/js/custom.js') }}\"></script>", "YLCoreAppBundle:Default:myHome.html.twig", "C:\\Users\\53680\\Documents\\Lendy\\lendy_api\\symfony\\src\\YL\\CoreAppBundle/Resources/views/Default/myHome.html.twig");
    }
}
