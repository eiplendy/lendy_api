<?php

/* YLCoreAppBundle:Default:editProfile.html.twig */
class __TwigTemplate_82a1d3222a1bccb50deb9a534b426dc10264b3d4b6ac145923feb0b953172af1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("YLCoreAppBundle::myLayout.html.twig", "YLCoreAppBundle:Default:editProfile.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "YLCoreAppBundle::myLayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:editProfile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:editProfile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo "    Titre
    -
    Marseille
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_header($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 9
        echo "
    <div class=\"wrapper\">
        <div class=\"profiledetails_sec pattern-header\">
            <div class=\"container\">
                <h2>

                   Votre profil

                </h2>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 24
        echo "<div class=\"d_profilein\">
    <div class=\"d_profile_mid\">
        <h2>À Propos de vous</h2>
        <div class=\"d_profile_mid_form\">

            <form name=\"a_propos_de_vous_form\" method=\"post\" onsubmit=\"return validateForm()\">
                <div class=\" d_profile_mid_form_sub\">
                    <p>Titre de votre annonce<sup class=\"required\">*</sup>
                    </p>
                    <input type=\"text\" id=\"a_propos_de_vous_form_user_telephone\" name=\"a_propos_de_vous_form[user][telephone]\" placeholder=\"ex : Super chauffeur !\" >

                    <!-- <br> <br> <br> -->

                </div>
                <div class=\" d_profile_mid_form_sub\">
                    <p>Description<sup class=\"required\">*</sup>
                    </p>
                    <textarea id=\"description_nounou_form_content\" name=\"description_nounou_form[content]\" required=\"required\" placeholder=\"Expliquez votre besoin ici\" rows=\"6;\" minlength=\"100\" class=\"fieldWithCounter form-control\"></textarea>


                </div>
                <!-- <div class=\"form-group\"> -->
                <div class=\"d_profile_mid_form_sub\">
                    <p>Sélectionnez votre adresse ou code potal
                        <sup class=\"required\">*</sup>
                    </p>
                    <div id=\"locationField\">
                        <input type=\"text\" id=\"a_propos_de_vous_form_user_adresse\" name=\"a_propos_de_vous_form[user][adresse]\" required=\"required\" placeholder=\"Adresse ou code postal\" onfocus=\"geolocate\" class=\"form-control\" value=\"13001 Marseille, France\" autocomplete=\"off\">
                    </div>
                </div>
                <div id=\"inscription_error\">


                </div>
                <!-- </div> -->
                <div id=\"error_address\" style=\"display: none\">
                    <p class=\"alert alert-danger custom-alert\"><i class=\"ion-alert-circled\"></i>Veuillez sélectionner une adresse valide !</p>
                </div>


                <div class=\" d_profile_mid_form_sub\">
                    <p>Quel est votre date de naissance ?<sup class=\"required\">*</sup>
                    </p>
                    <input type=\"text\" id=\"a_propos_de_vous_form_dateAge\" name=\"a_propos_de_vous_form[dateAge]\" required=\"required\" class=\"datepicker form-control\" placeholder=\"jj/mm/aaaa\" value=\"10/09/1983\" autocomplete=\"off\">
                    <br>
                    <br>
                    <div id=\"error-date\" style=\"display: none\">
                        <p class=\"alert alert-danger custom-alert\"><i class=\"ion-alert-circled\"></i>
                            Vous devez avoir au moins 14 ans ou le format est invalide</p>
                    </div>

                    <!-- <br> <br> <br> -->

                </div>

                <div class=\"d_profile_mid_form_sub\">
                    <p>Calendrier de vos disponibilités</p>
                    <div class=\"d_avail_sec\">
                        <table>
                            <thead>
                            <tr>
                                <th scope=\"col\">&nbsp;</th>
                                <th scope=\"col\">Lun</th>
                                <th scope=\"col\">Mar</th>
                                <th scope=\"col\">Mer</th>
                                <th scope=\"col\">Jeu</th>
                                <th scope=\"col\">Ven</th>
                                <th scope=\"col\">Sam</th>
                                <th scope=\"col\">Dim</th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">6h - 9h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"debut-matine-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_1\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure1_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"debut-matine-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_2\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure1_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"debut-matine-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_3\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure1_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"debut-matine-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_4\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure1_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"debut-matine-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_5\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure1_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"debut-matine-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_6\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure1_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"debut-matine-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_7\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure1_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>

                                <td scope=\"row\">
                                    <div class=\"visible-xs\">9h - 12h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"fin-matine-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_1\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure2_1\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"MARDI\" id=\"fin-matine-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_2\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure2_2\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"MERCREDI\" id=\"fin-matine-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_3\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure2_3\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"JEUDI\" id=\"fin-matine-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_4\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure2_4\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"VENDREDI\" id=\"fin-matine-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_5\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure2_5\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"SAMEDI\" id=\"fin-matine-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_6\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure2_6\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"DIMANCHE\" id=\"fin-matine-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_7\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure2_7\"><span><span></span></span>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">12h - 14h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_1\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure3_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_2\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure3_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_3\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure3_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_4\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure3_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_5\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure3_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_6\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure3_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_7\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure3_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">14h - 16h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"apres-midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_1\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure4_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"apres-midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_2\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure4_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"apres-midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_3\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure4_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"apres-midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_4\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure4_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"apres-midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_5\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure4_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"apres-midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_6\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure4_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"apres-midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_7\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure4_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">16h - 18h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"fin-apres-midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_1\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure5_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"fin-apres-midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_2\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure5_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"fin-apres-midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_3\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure5_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"fin-apres-midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_4\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure5_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"fin-apres-midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_5\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure5_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"fin-apres-midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_6\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure5_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"fin-apres-midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_7\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure5_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">18h - 20h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"fin-journee-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_1\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure6_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"fin-journee-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_2\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure6_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"fin-journee-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_3\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure6_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"fin-journee-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_4\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure6_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"fin-journee-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_5\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure6_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"fin-journee-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_6\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure6_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"fin-journee-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_7\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure6_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">20h et +</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"soir-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_1\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure7_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"soir-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_2\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure7_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"soir-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_3\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure7_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"soir-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_4\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure7_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"soir-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_5\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure7_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"soir-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_6\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure7_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"soir-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_7\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure7_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class=\"d_profile_mid_form_sub\">
                    <p>
                        <strong>Joignez votre photo!</strong>
                    </p>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-sm-6\">
                            <div class=\"form-group\">
                                <div id=\"mySnippet\">
                                    <div class=\"slim\"
                                         data-ratio=\"1:1\"
                                         data-label=\"Cliquez ici pour ajouter une image\"
                                         data-button-confirm-label=\"Confirmer\"
                                         data-button-cancel-label=\"Annuler\"
                                         data-max-file-size=\"5\"
                                         data-status-file-size=\"L'image est trop lourde, le maximum est de 5 Mo\"
                                         data-size=\"800,800\">
                                        <input type=\"file\" name=\"slim[]\" id=\"myCropper\">
                                    </div>
                                </div>
                                <p class=\"red-txt\" id=\"tooBigFile\" style=\"display: none\">Vous ne pouvez pas ajouter des photos de plus de 5 Mo, veuillez ajouter un autre fichier</p>
                            </div>
                        </div>

                    </div>

                </div>
                    <button type=\"submit\" id=\"a_propos_de_vous_form_Suivant\" name=\"a_propos_de_vous_form[Suivant]\" data-placeholder=\"Continuer\" class=\"plain-btn fullw-btn orange-btn btn\">Suivant</button>

            </form>

            <div class=\"clear\"></div>

        </div>
    </div>
</div>

    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.datepicker').datepicker({language: 'fr-FR', format: 'dd/mm/yyyy'});
            var cropper = \$('#mySnippet').slim('parse');
        });
    </script>
    <script>
    function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    (document.getElementById('a_propos_de_vous_form_user_adresse')),
    {
    types: ['geocode'],
    componentRestrictions: {
    country: [
    \"fr\",
    \"gp\",
    \"mq\",
    \"gf\",
    \"re\",
    \"pm\",
    \"yt\",
    \"nc\",
    \"pf\",
    \"mf\",
    \"tf\"
    ]
    }
    }
    );
    // When the user selects an address from the dropdown, populate the address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    }

    // [START region_fillform]
    function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
    }
    // Get each component of the address from the place details and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
    var val = place.address_components[i][componentForm[addressType]];
    document.getElementById(addressType).value = val;
    }
    document.getElementById('cityLat').value = place.geometry.location.lat();
    document.getElementById('cityLng').value = place.geometry.location.lng();
    }
    }

    // [END region_fillform] [START region_geolocation] Bias the autocomplete object to the user's geographical location, as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
    var geolocation = {
    lat: position.coords.latitude,
    lng: position.coords.longitude
    };
    var circle = new google.maps.Circle({center: geolocation, radius: position.coords.accuracy});
    autocomplete.setBounds(circle.getBounds());
    });
    }
    }

    // [END region_geolocation]
    </script>

    <script>

    </script>

    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCztRpaXkxh9IhAns4buznZi1eAnXKj_fA&signed_in=true&libraries=places&callback=initAutocomplete\" async=\"async\" defer=\"defer\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "YLCoreAppBundle:Default:editProfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 24,  95 => 23,  73 => 9,  64 => 8,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"YLCoreAppBundle::myLayout.html.twig\" %}
{% block title %}
    Titre
    -
    Marseille
{% endblock %}

{% block header %}

    <div class=\"wrapper\">
        <div class=\"profiledetails_sec pattern-header\">
            <div class=\"container\">
                <h2>

                   Votre profil

                </h2>
            </div>
        </div>
    </div>
{% endblock %}

{% block body %}
<div class=\"d_profilein\">
    <div class=\"d_profile_mid\">
        <h2>À Propos de vous</h2>
        <div class=\"d_profile_mid_form\">

            <form name=\"a_propos_de_vous_form\" method=\"post\" onsubmit=\"return validateForm()\">
                <div class=\" d_profile_mid_form_sub\">
                    <p>Titre de votre annonce<sup class=\"required\">*</sup>
                    </p>
                    <input type=\"text\" id=\"a_propos_de_vous_form_user_telephone\" name=\"a_propos_de_vous_form[user][telephone]\" placeholder=\"ex : Super chauffeur !\" >

                    <!-- <br> <br> <br> -->

                </div>
                <div class=\" d_profile_mid_form_sub\">
                    <p>Description<sup class=\"required\">*</sup>
                    </p>
                    <textarea id=\"description_nounou_form_content\" name=\"description_nounou_form[content]\" required=\"required\" placeholder=\"Expliquez votre besoin ici\" rows=\"6;\" minlength=\"100\" class=\"fieldWithCounter form-control\"></textarea>


                </div>
                <!-- <div class=\"form-group\"> -->
                <div class=\"d_profile_mid_form_sub\">
                    <p>Sélectionnez votre adresse ou code potal
                        <sup class=\"required\">*</sup>
                    </p>
                    <div id=\"locationField\">
                        <input type=\"text\" id=\"a_propos_de_vous_form_user_adresse\" name=\"a_propos_de_vous_form[user][adresse]\" required=\"required\" placeholder=\"Adresse ou code postal\" onfocus=\"geolocate\" class=\"form-control\" value=\"13001 Marseille, France\" autocomplete=\"off\">
                    </div>
                </div>
                <div id=\"inscription_error\">


                </div>
                <!-- </div> -->
                <div id=\"error_address\" style=\"display: none\">
                    <p class=\"alert alert-danger custom-alert\"><i class=\"ion-alert-circled\"></i>Veuillez sélectionner une adresse valide !</p>
                </div>


                <div class=\" d_profile_mid_form_sub\">
                    <p>Quel est votre date de naissance ?<sup class=\"required\">*</sup>
                    </p>
                    <input type=\"text\" id=\"a_propos_de_vous_form_dateAge\" name=\"a_propos_de_vous_form[dateAge]\" required=\"required\" class=\"datepicker form-control\" placeholder=\"jj/mm/aaaa\" value=\"10/09/1983\" autocomplete=\"off\">
                    <br>
                    <br>
                    <div id=\"error-date\" style=\"display: none\">
                        <p class=\"alert alert-danger custom-alert\"><i class=\"ion-alert-circled\"></i>
                            Vous devez avoir au moins 14 ans ou le format est invalide</p>
                    </div>

                    <!-- <br> <br> <br> -->

                </div>

                <div class=\"d_profile_mid_form_sub\">
                    <p>Calendrier de vos disponibilités</p>
                    <div class=\"d_avail_sec\">
                        <table>
                            <thead>
                            <tr>
                                <th scope=\"col\">&nbsp;</th>
                                <th scope=\"col\">Lun</th>
                                <th scope=\"col\">Mar</th>
                                <th scope=\"col\">Mer</th>
                                <th scope=\"col\">Jeu</th>
                                <th scope=\"col\">Ven</th>
                                <th scope=\"col\">Sam</th>
                                <th scope=\"col\">Dim</th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">6h - 9h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"debut-matine-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_1\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure1_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"debut-matine-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_2\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure1_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"debut-matine-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_3\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure1_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"debut-matine-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_4\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure1_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"debut-matine-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_5\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure1_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"debut-matine-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_6\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure1_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"debut-matine-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure1_7\" name=\"disponibilite_nounou_form[Heure1][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure1_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>

                                <td scope=\"row\">
                                    <div class=\"visible-xs\">9h - 12h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"fin-matine-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_1\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure2_1\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"MARDI\" id=\"fin-matine-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_2\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure2_2\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"MERCREDI\" id=\"fin-matine-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_3\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure2_3\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"JEUDI\" id=\"fin-matine-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_4\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure2_4\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"VENDREDI\" id=\"fin-matine-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_5\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure2_5\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"SAMEDI\" id=\"fin-matine-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_6\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure2_6\"><span><span></span></span>
                                    </label>
                                </td>
                                <td data-label=\"DIMANCHE\" id=\"fin-matine-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure2_7\" name=\"disponibilite_nounou_form[Heure2][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure2_7\"><span><span></span></span>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">12h - 14h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_1\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure3_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_2\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure3_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_3\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure3_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_4\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure3_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_5\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure3_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_6\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure3_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure3_7\" name=\"disponibilite_nounou_form[Heure3][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure3_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">14h - 16h</div>
                                </td>
                                <td data-label=\"LUNDI\" id=\"apres-midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_1\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure4_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"apres-midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_2\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure4_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"apres-midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_3\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure4_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"apres-midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_4\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure4_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"apres-midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_5\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure4_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"apres-midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_6\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure4_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"apres-midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure4_7\" name=\"disponibilite_nounou_form[Heure4][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure4_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">16h - 18h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"fin-apres-midi-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_1\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure5_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"fin-apres-midi-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_2\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure5_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"fin-apres-midi-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_3\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure5_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"fin-apres-midi-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_4\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure5_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"fin-apres-midi-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_5\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure5_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"fin-apres-midi-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_6\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure5_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"fin-apres-midi-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure5_7\" name=\"disponibilite_nounou_form[Heure5][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure5_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">18h - 20h</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"fin-journee-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_1\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure6_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"fin-journee-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_2\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure6_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"fin-journee-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_3\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure6_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"fin-journee-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_4\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure6_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"fin-journee-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_5\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure6_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"fin-journee-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_6\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure6_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"fin-journee-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure6_7\" name=\"disponibilite_nounou_form[Heure6][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure6_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            <tr>
                                <td scope=\"row\">
                                    <div class=\"visible-xs\">20h et +</div>
                                </td>

                                <td data-label=\"LUNDI\" id=\"soir-day-1\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_1\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"1\">
                                    <label for=\"disponibilite_nounou_form_Heure7_1\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MARDI\" id=\"soir-day-2\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_2\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"2\">
                                    <label for=\"disponibilite_nounou_form_Heure7_2\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"MERCREDI\" id=\"soir-day-3\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_3\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"3\">
                                    <label for=\"disponibilite_nounou_form_Heure7_3\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"JEUDI\" id=\"soir-day-4\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_4\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"4\">
                                    <label for=\"disponibilite_nounou_form_Heure7_4\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"VENDREDI\" id=\"soir-day-5\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_5\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"5\">
                                    <label for=\"disponibilite_nounou_form_Heure7_5\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"SAMEDI\" id=\"soir-day-6\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_6\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"6\">
                                    <label for=\"disponibilite_nounou_form_Heure7_6\"><span><span></span></span>
                                    </label></td>
                                <td data-label=\"DIMANCHE\" id=\"soir-day-7\">
                                    <input type=\"checkbox\" id=\"disponibilite_nounou_form_Heure7_7\" name=\"disponibilite_nounou_form[Heure7][]\" value=\"7\">
                                    <label for=\"disponibilite_nounou_form_Heure7_7\"><span><span></span></span>
                                    </label></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class=\"d_profile_mid_form_sub\">
                    <p>
                        <strong>Joignez votre photo!</strong>
                    </p>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-sm-6\">
                            <div class=\"form-group\">
                                <div id=\"mySnippet\">
                                    <div class=\"slim\"
                                         data-ratio=\"1:1\"
                                         data-label=\"Cliquez ici pour ajouter une image\"
                                         data-button-confirm-label=\"Confirmer\"
                                         data-button-cancel-label=\"Annuler\"
                                         data-max-file-size=\"5\"
                                         data-status-file-size=\"L'image est trop lourde, le maximum est de 5 Mo\"
                                         data-size=\"800,800\">
                                        <input type=\"file\" name=\"slim[]\" id=\"myCropper\">
                                    </div>
                                </div>
                                <p class=\"red-txt\" id=\"tooBigFile\" style=\"display: none\">Vous ne pouvez pas ajouter des photos de plus de 5 Mo, veuillez ajouter un autre fichier</p>
                            </div>
                        </div>

                    </div>

                </div>
                    <button type=\"submit\" id=\"a_propos_de_vous_form_Suivant\" name=\"a_propos_de_vous_form[Suivant]\" data-placeholder=\"Continuer\" class=\"plain-btn fullw-btn orange-btn btn\">Suivant</button>

            </form>

            <div class=\"clear\"></div>

        </div>
    </div>
</div>

    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.datepicker').datepicker({language: 'fr-FR', format: 'dd/mm/yyyy'});
            var cropper = \$('#mySnippet').slim('parse');
        });
    </script>
    <script>
    function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    (document.getElementById('a_propos_de_vous_form_user_adresse')),
    {
    types: ['geocode'],
    componentRestrictions: {
    country: [
    \"fr\",
    \"gp\",
    \"mq\",
    \"gf\",
    \"re\",
    \"pm\",
    \"yt\",
    \"nc\",
    \"pf\",
    \"mf\",
    \"tf\"
    ]
    }
    }
    );
    // When the user selects an address from the dropdown, populate the address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    }

    // [START region_fillform]
    function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
    }
    // Get each component of the address from the place details and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
    var val = place.address_components[i][componentForm[addressType]];
    document.getElementById(addressType).value = val;
    }
    document.getElementById('cityLat').value = place.geometry.location.lat();
    document.getElementById('cityLng').value = place.geometry.location.lng();
    }
    }

    // [END region_fillform] [START region_geolocation] Bias the autocomplete object to the user's geographical location, as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
    var geolocation = {
    lat: position.coords.latitude,
    lng: position.coords.longitude
    };
    var circle = new google.maps.Circle({center: geolocation, radius: position.coords.accuracy});
    autocomplete.setBounds(circle.getBounds());
    });
    }
    }

    // [END region_geolocation]
    </script>

    <script>

    </script>

    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCztRpaXkxh9IhAns4buznZi1eAnXKj_fA&signed_in=true&libraries=places&callback=initAutocomplete\" async=\"async\" defer=\"defer\"></script>
{% endblock %}", "YLCoreAppBundle:Default:editProfile.html.twig", "C:\\Users\\53680\\Documents\\Lendy\\lendy_api\\symfony\\src\\YL\\CoreAppBundle/Resources/views/Default/editProfile.html.twig");
    }
}
