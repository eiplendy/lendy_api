<?php

/* YLCoreAppBundle:Default:myRegister.html.twig */
class __TwigTemplate_a2f331098a4477ba7804fe6139c0aa552dbee7ab13fa5e761b006c0845f44de8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:myRegister.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "YLCoreAppBundle:Default:myRegister.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"author\" content=\"ThemeStarz\">

    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Poppins:400,500,600\">
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/bootstrap/css/bootstrap.min.css "), "html", null, true);
        echo " \">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/font-awesome/css/fontawesome-all.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/style.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/owl.carousel.min.css"), "html", null, true);
        echo "\">
    <title>Lendy</title>

</head>
<body data-spy=\"scroll\" data-target=\".navbar\" class=\"\">
<div class=\"ts-page-wrapper\" id=\"page-top\">
    <nav class=\"navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element in\" data-bg-color=\"#1a1360\">
        <div class=\"container\">
            <a class=\"navbar-brand\" href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_myhome");
        echo "\">
                <img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/logopetit.png"), "html", null, true);
        echo "\" alt=\"\">
            </a>
            <!--end navbar-brand-->
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>
            <!--end navbar-toggler-->
            <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
                <div class=\"navbar-nav ml-auto\">
                    <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_myLogin");
        echo "\">Se connecter</a>
                    <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_register_choice");
        echo "\">S'inscrire</a>
                </div>
                <!--end navbar-nav-->
            </div>
            <!--end collapse-->
        </div>
        <!--end container-->
    </nav>
    <!--*********************************************************************************************************-->
    <!--************ HERO ***************************************************************************************-->
    <!--*********************************************************************************************************-->
    <!--end #hero-->

    <!--*********************************************************************************************************-->
    <!--************ CONTENT ************************************************************************************-->
    <!--*********************************************************************************************************-->
    <main id=\"ts-content\">

        <!--HOW IT WORKS ****************************************************************************************-->
        ";
        // line 88
        echo "        <!--END HOW IT WORKS ************************************************************************************-->

        <input type=\"radio\" checked id=\"toggle--login\" name=\"toggle\" class=\"ghost-login\" />
        <input type=\"radio\" id=\"toggle--signup\" name=\"toggle\" class=\"ghost-login\" />

        <img class=\"logo-login framed-login\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/logo-moyen.png"), "html", null, true);
        echo "\" alt=\"Lendy logo\" />

        <div class=\"form-login form--signup-login framed-login\">
            <h2 class=\"text-login text--centered-login text--omega-login\">Rejoignez la communauté Lendy.</h2>

            ";
        // line 98
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 99
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nom", array()), 'row');
        echo "
            ";
        // line 100
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "prenom", array()), 'row');
        echo "
            ";
        // line 101
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row');
        echo "
            ";
        // line 102
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "password", array()), 'row', array("type" => "password"));
        echo "

            <button type=\"submit\">Register!</button>
            ";
        // line 105
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

            <label for=\"toggle--login\" class=\"text-login text--small-login text--centered-login\">Déja inscrit? <b><a href=\"";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("yl_core_app_myLogin");
        echo "\">Connectez vous!</a></b></label>
        </div>


        <div class=\"fullscreen-bg-register\"></div>

    </main>
    <!--end #content-->

</div>
<!--end page-->

<script>
    if( document.getElementsByClassName(\"ts-full-screen\").length ) {
        document.getElementsByClassName(\"ts-full-screen\")[0].style.height = window.innerHeight + \"px\";
    }
</script>
<script src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/popper.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/imagesloaded.pkgd.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/isInViewport.jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.particleground.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/scrolla.jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-validate.bootstrap-tooltip.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js\"></script>
<script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.wavify.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/custom.js"), "html", null, true);
        echo "\"></script>


</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "YLCoreAppBundle:Default:myRegister.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 137,  203 => 135,  198 => 133,  194 => 132,  190 => 131,  186 => 130,  182 => 129,  178 => 128,  174 => 127,  170 => 126,  166 => 125,  162 => 124,  142 => 107,  137 => 105,  131 => 102,  127 => 101,  123 => 100,  119 => 99,  115 => 98,  107 => 93,  100 => 88,  78 => 31,  74 => 30,  62 => 21,  58 => 20,  47 => 12,  43 => 11,  39 => 10,  35 => 9,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"author\" content=\"ThemeStarz\">

    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Poppins:400,500,600\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/bootstrap/css/bootstrap.min.css ') }} \">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/font-awesome/css/fontawesome-all.min.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/style.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/owl.carousel.min.css') }}\">
    <title>Lendy</title>

</head>
<body data-spy=\"scroll\" data-target=\".navbar\" class=\"\">
<div class=\"ts-page-wrapper\" id=\"page-top\">
    <nav class=\"navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element in\" data-bg-color=\"#1a1360\">
        <div class=\"container\">
            <a class=\"navbar-brand\" href=\"{{ path('yl_core_app_myhome') }}\">
                <img src=\"{{ asset('assets/img/logopetit.png') }}\" alt=\"\">
            </a>
            <!--end navbar-brand-->
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>
            <!--end navbar-toggler-->
            <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
                <div class=\"navbar-nav ml-auto\">
                    <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"{{ path('yl_core_app_myLogin') }}\">Se connecter</a>
                    <a class=\"nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto\" href=\"{{ path('yl_core_app_register_choice') }}\">S'inscrire</a>
                </div>
                <!--end navbar-nav-->
            </div>
            <!--end collapse-->
        </div>
        <!--end container-->
    </nav>
    <!--*********************************************************************************************************-->
    <!--************ HERO ***************************************************************************************-->
    <!--*********************************************************************************************************-->
    <!--end #hero-->

    <!--*********************************************************************************************************-->
    <!--************ CONTENT ************************************************************************************-->
    <!--*********************************************************************************************************-->
    <main id=\"ts-content\">

        <!--HOW IT WORKS ****************************************************************************************-->
        {#<section id=\"how-it-works\" class=\"ts-block text-center\">
            <div class=\"container\">
                <!--end ts-title-->
                <div class=\"row\">
                    <div class=\"col-sm-6 col-md-6 col-xl-6\">
                        <figure data-animate=\"ts-fadeInUp\">
                            <figure class=\"icon mb-5 p-2\">
                                <img src=\"{{ asset('assets/img/wheel.png') }}\" alt=\"\">
                                <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"{{ asset('assets/svg/organic-shape-01.svg') }}\"></div>
                            </figure>
                            <h4>Je suis chauffeur</h4>
                            <p>
                                Vous avez du temps à consacrer à la plateforme, et vous voulez récuperer un véhicule en échange?
                            </p>
                            <a href=\"#\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                        </figure>
                    </div>
                    <!--end col-xl-4-->
                    <div class=\"col-sm-6 col-md-6 col-xl-6\">
                        <figure data-animate=\"ts-fadeInUp\" data-delay=\"0.1s\">
                            <figure class=\"icon mb-5 p-2\">
                                <img src=\"{{ asset('assets/img/handshake.png') }}\" alt=\"\">
                                <div class=\"ts-svg\" data-animate=\"ts-zoomInShort\" data-bg-image=\"{{ asset('assets/svg/organic-shape-02.svg') }}\"></div>
                            </figure>
                            <h4>Je suis prêteur</h4>
                            <p>
                                Vous avez un véhicule à dispotition mais vous l'utilsez rarement. Prêtez le en échange d'un chauffeur !
                            </p>
                            <a href=\"#\" class=\"btn btn-primary mb-4 ts-scroll\">S'inscrire</a>
                        </figure>
                    </div>
                    <!--end col-xl-4-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>#}
        <!--END HOW IT WORKS ************************************************************************************-->

        <input type=\"radio\" checked id=\"toggle--login\" name=\"toggle\" class=\"ghost-login\" />
        <input type=\"radio\" id=\"toggle--signup\" name=\"toggle\" class=\"ghost-login\" />

        <img class=\"logo-login framed-login\" src=\"{{ asset('assets/img/logo-moyen.png') }}\" alt=\"Lendy logo\" />

        <div class=\"form-login form--signup-login framed-login\">
            <h2 class=\"text-login text--centered-login text--omega-login\">Rejoignez la communauté Lendy.</h2>

            {{ form_start(form) }}
            {{ form_row(form.nom) }}
            {{ form_row(form.prenom) }}
            {{ form_row(form.email) }}
            {{ form_row(form.password, { 'type': 'password' }) }}

            <button type=\"submit\">Register!</button>
            {{ form_end(form) }}

            <label for=\"toggle--login\" class=\"text-login text--small-login text--centered-login\">Déja inscrit? <b><a href=\"{{ path('yl_core_app_myLogin') }}\">Connectez vous!</a></b></label>
        </div>


        <div class=\"fullscreen-bg-register\"></div>

    </main>
    <!--end #content-->

</div>
<!--end page-->

<script>
    if( document.getElementsByClassName(\"ts-full-screen\").length ) {
        document.getElementsByClassName(\"ts-full-screen\")[0].style.height = window.innerHeight + \"px\";
    }
</script>
<script src=\"{{ asset('assets/js/jquery-3.3.1.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/popper.min.js') }}\"></script>
<script src=\"{{ asset('assets/bootstrap/js/bootstrap.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/imagesloaded.pkgd.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/isInViewport.jquery.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery.particleground.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/owl.carousel.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/scrolla.jquery.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery.validate.min.js') }}\"></script>
<script src=\"{{ asset('assets/js/jquery-validate.bootstrap-tooltip.min.js') }}\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js\"></script>
<script src=\"{{ asset('assets/js/jquery.wavify.js') }}\"></script>

<script src=\"{{ asset('assets/js/custom.js') }}\"></script>


</body>
</html>
", "YLCoreAppBundle:Default:myRegister.html.twig", "C:\\Users\\53680\\Documents\\Lendy\\lendy_api\\symfony\\src\\YL\\CoreAppBundle/Resources/views/Default/myRegister.html.twig");
    }
}
